#ifndef __RSA_UTILS_H_H__
#define __RSA_UTILS_H_H__
#include <string>

class RsaUtils
{
private:
	enum PKCS_TYPE {
		PKCS_1 = 0,  // PKCS#1  起止是 -----BEGIN RSA PRIVATE KEY----- 和 -----END RSA PRIVATE KEY----- 公钥即是把PRIVATE换成PUBLIC
		PKCS_8 = 8   // PKCS#8  起止是 -----BEGIN PRIVATE KEY----- 和 -----END PRIVATE KEY----- 公钥即是把PRIVATE换成PUBLIC
	};

public:
	static void generate_rsa_key(std::string &out_pub_key, std::string &out_pri_key);

	static std::string encrypt_by_privkey_file(const char *priv_key_file, const std::string &content);
	static std::string decrypt_by_pubkey_file(const char *priv_key_file, const std::string &cipher_text, PKCS_TYPE pkcs_type);

	static std::string encrypt_by_privkey(std::string priv_key, const std::string &content);
	static std::string decrypt_by_pubkey(std::string pub_key, const std::string &cipher_text, PKCS_TYPE pkcs_type);

	static std::string sign_key_file(const char *priv_key_file, const std::string &content);
	static bool verify_key_file(const char *pub_key_file, const std::string &content, const std::string& sign, PKCS_TYPE pkcs_type);

	static std::string sign(std::string priv_key, const std::string &content);
	 
	// call RsaUtils::PKCS1() or RsaUtils::PKCS8() to get the type enum;
	static bool verify(std::string pub_key, const std::string &content, const std::string& sign, PKCS_TYPE pkcs_type);
	
	

	static PKCS_TYPE PKCS1();
	static PKCS_TYPE PKCS8();
};

#endif // __RSA_UTILS_H_H__

/*
 * 密钥生成：
 * OpenSSL> genrsa -out rsa_private_key.pem   1024  #生成私钥
 * OpenSSL> pkcs8 -topk8 -inform PEM -in rsa_private_key.pem -outform PEM -nocrypt -out rsa_private_key_pkcs8.pem #Java开发者需要将私钥转换成PKCS8格式
 * OpenSSL> rsa -in rsa_private_key.pem -pubout -out rsa_public_key.pem #生成公钥 pkcs#8格式
 * OpenSSL> exit #退出OpenSSL程序
 *  Java中使用 rsa_private_key_pkcs8.pem 作为私钥，而且需要把 -----BEGIN PRIVATE KEY----- 和 -----END PRIVATE KEY----- 去掉
 *  C/C++ 中 两个都可以作为私钥
*/

/*
 * PKCS#1 和 PKCS#8 是两种格式
 *	PKCS#1 的 起止是 -----BEGIN RSA PRIVATE KEY----- 和 -----END RSA PRIVATE KEY-----
 *	PKCS#8 的 起止是 -----BEGIN PRIVATE KEY----- 和 -----END PRIVATE KEY-----
 *  上述是私钥，公钥是把 PRIVATE 换成 PUBLIC
 * 
 * 两种格式的私钥在读取时用同一个函数：
 *	PEM_read_bio_RSAPrivateKey
 *
 * 但是在读取公钥时，两种格式需要使用不同的函数：
 *	PKCS#1 : PEM_read_bio_RSAPublicKey
 *	PKCS#8 : PEM_read_bio_RSA_PUBKEY
*/

/*
 * 有的私钥不包含-----BEGIN PRIVATE KEY----- 和 -----END PRIVATE KEY-----
 * 在 C/C++中 要加上，包括\n
 * 公钥同理
*/
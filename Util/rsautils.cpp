#include "rsautils.h"
#include "base64_tool.h"
#include <fstream>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/err.h>
#pragma comment(lib,"libssl.lib")
#pragma comment(lib,"libcrypto.lib")

#define KEY_LENGTH  2048             // 密钥长度
#define PUB_KEY_FILE "pubkey.pem"    // 公钥路径
#define PRI_KEY_FILE "prikey.pem"    // 私钥路径

RsaUtils::PKCS_TYPE RsaUtils::PKCS1() { return RsaUtils::PKCS_TYPE::PKCS_1; }
RsaUtils::PKCS_TYPE RsaUtils::PKCS8() { return RsaUtils::PKCS_TYPE::PKCS_8; }

static std::string get_file_data(const char *filename, char buf[], int bufsize)
{
	FILE* fp = fopen(filename, "rb");
	if (!fp)
		return "";

	int nread = fread(buf, 1, bufsize, fp);
	fclose(fp);
	std::string data(buf, buf + nread);
	return data;
}

std::string RsaUtils::sign_key_file(const char *priv_key_file, const std::string &content)
{
	char buf[10240] = { 0 };
	int bufsize = 10240;
	std::string priv_key = get_file_data(priv_key_file, buf, bufsize);

	return RsaUtils::sign(priv_key, content);
}

bool RsaUtils::verify_key_file(const char *pub_key_file, const std::string &content, const std::string& sign, PKCS_TYPE pkcs_type)
{
	char buf[10240] = { 0 };
	int bufsize = 10240;
	std::string pub_key = get_file_data(pub_key_file, buf, bufsize);

	return RsaUtils::verify(pub_key, content, sign, pkcs_type);
}

std::string RsaUtils::encrypt_by_privkey_file(const char *priv_key_file, const std::string &content)
{
	char buf[10240] = { 0 };
	int bufsize = 10240;
	std::string priv_key = get_file_data(priv_key_file, buf, bufsize);
	return RsaUtils::encrypt_by_privkey(priv_key, content);
}

std::string RsaUtils::decrypt_by_pubkey_file(const char *pub_key_file, const std::string &cipher_text, PKCS_TYPE pkcs_type)
{
	char buf[10240] = { 0 };
	int bufsize = 10240;
	std::string pub_key = get_file_data(pub_key_file, buf, bufsize);
	return RsaUtils::decrypt_by_pubkey(pub_key, cipher_text, pkcs_type);
}

std::string RsaUtils::sign(std::string priv_key, const std::string &content)
{
	BIO *bufio = NULL;
	RSA *rsa = NULL;
	EVP_PKEY *evpKey = NULL;
	EVP_MD_CTX *ctx = EVP_MD_CTX_create();
	int result = 0;
	unsigned int size = 0;
	char *sign = NULL;
	std::string signStr = "";

	/*
	 * private key is in memory as string, use: 
			BIO *bio = BIO_new_mem_buf(priv_key, key_len);
			RSA *rsa = PEM_read_bio_RSAPrivateKey(bio, NULL, NULL, NULL);
	   private key is in file, use:
			BIO *bufio = BIO_new(BIO_s_file());
			BIO_read_filename(bufio, "rsa_private_key.pem");
			RSA *rsa = PEM_read_bio_RSAPrivateKey(bufio, NULL, NULL, NULL);
	*/

	bufio = BIO_new_mem_buf(priv_key.data(), priv_key.length());
	if (bufio == NULL) {  
		printf("BIO_new_mem_buf failed");
		goto safe_exit;
	}  
	
	//bufio = BIO_new(BIO_s_file());
	//BIO_read_filename(bufio, "rsa_private_key.pem");  

	rsa = PEM_read_bio_RSAPrivateKey(bufio, NULL, NULL, NULL);
	if (rsa == NULL) {
		printf("PEM_read_bio_RSAPrivateKey failed");
		goto safe_exit;
	}

	evpKey = EVP_PKEY_new();
	if (evpKey == NULL) {
		printf("EVP_PKEY_new failed");
	}

	if ((result = EVP_PKEY_set1_RSA(evpKey, rsa)) != 1) {
		printf("EVP_PKEY_set1_RSA failed");
		goto safe_exit;
	}

	EVP_MD_CTX_init(ctx);

	/* EVP_SignInit_ex 的第二个参数为指定签名的算法
		  java:		SHA1WithRSA   MD5withRSA
		  C/C++:	EVP_sha1()    EVP_md5() 
	*/
	if (result == 1 && (result = EVP_SignInit_ex(ctx,
		EVP_md5(), NULL)) != 1) {
	}

	if (result == 1 && (result = EVP_SignUpdate(ctx,
		content.c_str(), content.size())) != 1) {
		printf("EVP_SignUpdate failed");
	}

	size = EVP_PKEY_size(evpKey);
	sign = (char*)malloc(size + 1);
	memset(sign, 0, size + 1);

	if (result == 1 && (result = EVP_SignFinal(ctx,
		(unsigned char*)sign,
		&size, evpKey)) != 1) {
		printf("EVP_SignFinal failed");
	}

	signStr.assign(sign, sign + size);
	EVP_MD_CTX_destroy(ctx);
	free(sign);

safe_exit:
	if (rsa != NULL) {
		RSA_free(rsa);
		rsa = NULL;
	}

	if (evpKey != NULL) {
		EVP_PKEY_free(evpKey);
		evpKey = NULL;
	}

	if (bufio != NULL) {
		BIO_free_all(bufio);
		bufio = NULL;
	}

	return signStr;
}

bool RsaUtils::verify(std::string pub_key, const std::string &content, const std::string& sign, PKCS_TYPE pkcs_type)
{
	RSA *rsa = NULL;
	BIO *bufio = NULL;
	EVP_PKEY *evpKey = NULL;
	bool verify = false;
	EVP_MD_CTX *ctx = EVP_MD_CTX_create();
	int result = 0;
	unsigned char sign_decode[1024] = { 0 };
	int dlen = base64_decode((unsigned char *)sign.data(), sign.length(), (unsigned char *)sign_decode);
	std::string decodedSign(sign_decode, sign_decode + dlen);
	char *chDecodedSign = const_cast<char*>(decodedSign.c_str());

	bufio = BIO_new_mem_buf(pub_key.data(), pub_key.length());

	/*
	 *  读取公钥时，两种格式需要使用不同的函数：
	 *	 PKCS#1 : PEM_read_bio_RSAPublicKey
	 *	 PKCS#8 : PEM_read_bio_RSA_PUBKEY
	*/
	if (pkcs_type == PKCS1())
		rsa = PEM_read_bio_RSAPublicKey(bufio, NULL, NULL, NULL);
	else if (pkcs_type == PKCS8())
		rsa = PEM_read_bio_RSA_PUBKEY(bufio, NULL, NULL, NULL);
	else {
		printf("unknown pkcs type.\n");
		return false;
	}
	if (rsa == NULL) {
		printf("PEM_read_bio_RSA_PUBKEY failed");
		goto safe_exit;
	}

	evpKey = EVP_PKEY_new();
	if (evpKey == NULL) {
		printf("EVP_PKEY_new failed");
		goto safe_exit;
	}

	if ((result = EVP_PKEY_set1_RSA(evpKey, rsa)) != 1) {
		printf("EVP_PKEY_set1_RSA failed");
		goto safe_exit;
	}

	EVP_MD_CTX_init(ctx);

	if (result == 1 && (result = EVP_VerifyInit_ex(ctx,
		EVP_md5(), NULL)) != 1) {
		printf("EVP_VerifyInit_ex failed");
	}

	if (result == 1 && (result = EVP_VerifyUpdate(ctx,
		content.c_str(), content.size())) != 1) {
		printf("EVP_VerifyUpdate failed");
	}

	if (result == 1 && (result = EVP_VerifyFinal(ctx,
		(unsigned char*)chDecodedSign,
		decodedSign.size(), evpKey)) != 1) {
		printf("EVP_VerifyFinal failed");
	}
	if (result == 1) {
		verify = true;
	}
	else {
		printf("verify failed");
	}

	EVP_MD_CTX_destroy(ctx);

safe_exit:
	if (rsa != NULL) {
		RSA_free(rsa);
		rsa = NULL;
	}

	if (evpKey != NULL) {
		EVP_PKEY_free(evpKey);
		evpKey = NULL;
	}

	if (bufio != NULL) {
		BIO_free_all(bufio);
		bufio = NULL;
	}

	return verify;
}

std::string RsaUtils::encrypt_by_privkey(std::string priv_key, const std::string &content)
{
	std::string encrypt_text;
	BIO *keybio = BIO_new_mem_buf((unsigned char *)priv_key.c_str(), -1);
	RSA* rsa = RSA_new();
	rsa = PEM_read_bio_RSAPrivateKey(keybio, &rsa, NULL, NULL);
	if (!rsa)
	{
		BIO_free_all(keybio);
		return std::string("");
	}

	// 获取RSA单次可以处理的数据的最大长度
	int len = RSA_size(rsa);

	// 申请内存：存贮加密后的密文数据
	char *text = new char[len + 1];
	memset(text, 0, len + 1);

	// 对数据进行私钥加密（返回值是加密后数据的长度）
	int ret = RSA_private_encrypt(content.length(), (const unsigned char*)content.c_str(), (unsigned char*)text, rsa, RSA_PKCS1_PADDING);
	if (ret >= 0) {
		encrypt_text = std::string(text, ret);
	}

	// 释放内存  
	free(text);
	BIO_free_all(keybio);
	RSA_free(rsa);

	return encrypt_text;
}

std::string RsaUtils::decrypt_by_pubkey(std::string pub_key, const std::string &cipher_text, PKCS_TYPE pkcs_type)
{
	std::string decrypt_text;
	BIO *keybio = BIO_new_mem_buf((unsigned char *)pub_key.c_str(), -1);
	RSA *rsa = RSA_new();

	if (pkcs_type == PKCS1())
		rsa = PEM_read_bio_RSAPublicKey(keybio, &rsa, NULL, NULL);
	else if (pkcs_type == PKCS8())
		rsa = PEM_read_bio_RSA_PUBKEY(keybio, &rsa, NULL, NULL);
	else {
		printf("unknown pkcs type.\n");
		return false;
	}

	if (!rsa)
	{
		unsigned long err = ERR_get_error(); //获取错误号
		char err_msg[1024] = { 0 };
		ERR_error_string(err, err_msg); // 格式：error:errId:库:函数:原因
		printf("err msg: err:%ld, msg:%s\n", err, err_msg);
		BIO_free_all(keybio);
		return decrypt_text;
	}

	int len = RSA_size(rsa);
	char *text = new char[len + 1];
	memset(text, 0, len + 1);
	// 对密文进行解密, 此处的 padding 内容要统一
	int ret = RSA_public_decrypt(cipher_text.length(), (const unsigned char*)cipher_text.c_str(), (unsigned char*)text, rsa, RSA_PKCS1_PADDING);
	if (ret >= 0) {
		decrypt_text.append(std::string(text, ret));
	}

	// 释放内存  
	delete text;
	BIO_free_all(keybio);
	RSA_free(rsa);

	return decrypt_text;
}

void RsaUtils::generate_rsa_key(std::string &out_pub_key, std::string &out_pri_key)
{
	size_t pri_len = 0; // 私钥长度
	size_t pub_len = 0; // 公钥长度
	char *pri_key = nullptr; // 私钥
	char *pub_key = nullptr; // 公钥

	// 生成密钥对
	RSA *keypair = RSA_generate_key(KEY_LENGTH, RSA_3, NULL, NULL);

	BIO *pri = BIO_new(BIO_s_mem());
	BIO *pub = BIO_new(BIO_s_mem());

	// 生成私钥
	PEM_write_bio_RSAPrivateKey(pri, keypair, NULL, NULL, 0, NULL, NULL);
	// 注意------生成第1种格式的公钥
	//PEM_write_bio_RSAPublicKey(pub, keypair);
	// 注意------生成第2种格式的公钥（此处代码中使用这种）
	PEM_write_bio_RSA_PUBKEY(pub, keypair);

	// 获取长度  
	pri_len = BIO_pending(pri);
	pub_len = BIO_pending(pub);

	// 密钥对读取到字符串  
	pri_key = (char *)malloc(pri_len + 1);
	pub_key = (char *)malloc(pub_len + 1);

	BIO_read(pri, pri_key, pri_len);
	BIO_read(pub, pub_key, pub_len);

	pri_key[pri_len] = '\0';
	pub_key[pub_len] = '\0';

	out_pub_key = pub_key;
	out_pri_key = pri_key;

	// 将公钥写入文件
	std::ofstream pub_file(PUB_KEY_FILE, std::ios::out);
	if (!pub_file.is_open())
	{
		perror("pub key file open fail:");
		return;
	}
	pub_file << pub_key;
	pub_file.close();

	// 将私钥写入文件
	std::ofstream pri_file(PRI_KEY_FILE, std::ios::out);
	if (!pri_file.is_open())
	{
		perror("pri key file open fail:");
		return;
	}
	pri_file << pri_key;
	pri_file.close();

	// 释放内存
	RSA_free(keypair);
	BIO_free_all(pub);
	BIO_free_all(pri);

	free(pri_key);
	free(pub_key);
}